import scrapy
from scrapy_splash import SplashRequest
from inmuebles_Airbnb.items import InmueblesAirbnbItem
###########################################


class QuotesSpider(scrapy.Spider):
    name = "inmuebles_splash_spider"

    start_urls = ['https://www.airbnb.com.ar/s/Capital-Federal--CABA--Argentina/homes?refinement_paths%5B%5D=%2Fhomes&query=Capital%20Federal%2C%20CABA%2C%20Argentina&toddlers=0&click_referer=t%3ASEE_ALL%7Csid%3A49eac396-3bf1-441b-acad-83353f9ce88f%7Cst%3AMAGAZINE_HOMES&search_type=unknown&zoom=13&search_by_map=true&sw_lat=-34.648388232826434&sw_lng=-58.46968566192629&ne_lat=-34.559973774144254&ne_lng=-58.32334433807375&checkin=2019-08-01&checkout=2019-08-31&adults=1&place_id=ChIJvQz5TjvKvJURh47oiC6Bs6A&s_tag=9lZGaycc']


    def start_requests(self):
        for url in self.start_urls:
            yield SplashRequest(url=url, callback=self.parse, endpoint='render.html')

    """
    def find_items(self, response):
        xpath = '//div[contains(@class, "_8ssblpx")]'
        links = response.xpath(xpath).getall()
        print(links)


        for link in links:
            url = 'https://www.zonaprop.com.ar' + link
            print('|||||||||||| ' + str(url) + ' ||||||||||||')
            yield scrapy.Request(url=url, callback=self.parse)
    """

    # Función de scrapper
    def parse(self, response):
        print('-------------'+ response +'-------------')
        for q in response:

            # Link de la clase _8ssblpx
            print('-------------')
            item['link'] = response

            yield item





    """
    def parse(self, response):


        # Instancia del Item

        # Respuesta para parsear
        url = response





        # Título del anuncio
        title = response.xpath('//hgroup/h2//text()').get()
        item['titulo'] = title

        # Dirección del anuncio
        text = response.xpath('//hgroup/h2//text()').getall()

        if len(text) >= 2:
            adress = text[1]
        else:
            adress = []

        item['direccion'] = adress

        # Período mínimo de alquiler
        text2 = response.xpath('//div[@id="verDatosDescripcion"]//div[@align="center"]//text()').getall()

        if len(text2) >= 1:
            period = text[0]
        else:
            period = []

        item['periodo'] = period

        # Precio por período
        text3 = response.xpath('//div[@id="verDatosDescripcion"]//div[@align="center"]//text()').getall()

        if len(text3) >= 2:
            period_price = text[1]
        else:
            period_price = []

        item['precio_periodo'] = period_price

        # Precio publicado
        price = response.xpath('//div[@class="price-items"]/span/text()').extract_first()
        item['precio'] = price

        # Descripción
        description = response.xpath('//div[@id="verDatosDescripcion"]/text()').getall()
        item['descripcion'] = description

        # Tiempo que lleva publicado el anuncio
        published_date = ' '.join([' '.join(l.split()) for l in response.xpath('//h5[@class="section-date"]//text()').getall()])
        item['tiempo_publicado'] = published_date

        #Nombre, Código y contacto de anunciante
        publisher_name = response.xpath('//*[@class="publisher-subtitle"]//b').extract()[0]
        publisher_portal = 'https://www.zonaprop.com.ar' + response.xpath('//*[@class="column-left"]/a').extract()\
                            [0].split('href="')[1].split('"')[0]
        publisher_id = response.xpath('//*[@class="publisher-code"]/text()')[0].extract()
        item['anunciante'] = publisher_name
        item['anunciante_portal'] = publisher_portal
        item['anunciante_id'] = publisher_id

        # Código de la publicación
        publication_id = response.xpath('//*[@class="publisher-code"]/text()')[1].extract()
        item['publicacion_id'] = publication_id

        # Ubicación geográfica de la propiedad
        publication_geo = response.xpath("//*[contains(@id, 'static-map')]").extract()[0].split('markers=')[1].split('&amp;')[0]
        item['ubicacion_geo'] = publication_geo

        # Características de la propiedad
        i = 0
        result = []
        charact = response.xpath('//section[@class="general-section article-section"]//ul[@class="section-bullets"]/li//text()').getall()
        charact = [' '.join(ele.split()) for ele in charact]

        while i < len(charact):
            if i+1 < len(charact) and charact[i +1] == ':':
                result.append(charact[i]+charact[i+1]+charact[i+2])
                i += 2
            else:
                result.append(charact[i])
            i+=1

        characteristics = result
        item['caracteristicas'] = characteristics

        # URL y Path de las imágenes de las propiedades
        imgs_urls = response.xpath('//figure/img/@data-src').getall()
        item["image_urls"] = imgs_urls


        # URL de publicación
        print (url)
        item['url_publicacion'] = str(url).replace('<HtmlResponse 200 ', '').replace('>', '').replace('<200 ', '')

        """
